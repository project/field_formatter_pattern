## CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Maintainers

## INTRODUCTION

Field Formatter pattern provides custom HTML pattern for a textfield or
textarea and will only allow values as per that pattern.

  - For a full description of the module visit:
    <https://www.drupal.org/project/field_formatter_pattern>

  - To submit bug reports and feature suggestions, or to track changes
    visit:
    <https://www.drupal.org/project/issues/field_formatter_pattern>

## REQUIREMENTS

- This module requires no modules outside of Drupal core.

## INSTALLATION

  - Install the Field Formatter pattern module as you would normally
    install a contributed Drupal module. Visit
    <https://www.drupal.org/node/1897420> for further information.

## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types [Content type to
       edit] > Manage form display.
    3. Select the contextual links icon for the desired field and enter the
       Pattern.
    4. Click update and save.

## MAINTAINERS

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
