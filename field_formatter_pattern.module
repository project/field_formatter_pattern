<?php

/**
 * @file
 * Provides custom HTML pattern attribute settings for field formatters.
 */

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function field_formatter_pattern_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.field_formatter_pattern':
      $output = '<p>' . t('Provides custom HTML pattern attribute settings for field formatters.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_field_widget_third_party_settings_form().
 *
 * @todo Add the settings in the field schema and also test the edge values,
 * like 0 or negative.
 */
function field_formatter_pattern_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state) {
  $plugin_id = $plugin->getPluginId();
  // Depending on the widget, we may have different settings.
  $widget_settings = \Drupal::service('field_formatter_pattern.widget_settings');
  $allowed_settings = $widget_settings->getAllowedSettings($plugin_id);

  if (!empty($allowed_settings)) {
    $element = [];
    if (!empty($allowed_settings['pattern'])) {
      $element['pattern'] = [
        '#type' => 'textfield',
        '#title' => t('Pattern'),
        '#description' => t('The pattern for this field.'),
        '#default_value' => $plugin->getThirdPartySetting('field_formatter_pattern', 'pattern'),
      ];
      $element['pattern_error_message'] = [
        '#type' => 'textfield',
        '#title' => t('Pattern error message'),
        '#description' => t('The pattern error message for this field.'),
        '#default_value' => $plugin->getThirdPartySetting('field_formatter_pattern', 'pattern_error_message'),
      ];
    }
    return $element;
  }
}

/**
 * Implements hook_field_widget_settings_summary_alter().
 */
function field_formatter_pattern_field_widget_settings_summary_alter(&$summary, $context) {
  $plugin_id = $context['widget']->getPluginId();
  $widget_settings = \Drupal::service('field_formatter_pattern.widget_settings');
  $allowed_settings = $widget_settings->getAllowedSettings($plugin_id);
  if (!empty($allowed_settings)) {
    if (!empty($allowed_settings['pattern']) && $context['widget']->getThirdPartySetting('field_formatter_pattern', 'pattern')) {
      $summary[] = t('Pattern: @pattern', ['@pattern' => $context['widget']->getThirdPartySetting('field_formatter_pattern', 'pattern')]);
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function field_formatter_pattern_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  $thirdPartySettings = $context['widget']->getThirdPartySettings();
  if (!empty($thirdPartySettings['field_formatter_pattern']['pattern'])) {
    $element['value']['#attributes']['pattern'] = $thirdPartySettings['field_formatter_pattern']['pattern'];
  }
  if (!empty($thirdPartySettings['field_formatter_pattern']['pattern_error_message'])) {
    $element['value']['#attributes']['title'] = $thirdPartySettings['field_formatter_pattern']['pattern_error_message'];
  }
}
