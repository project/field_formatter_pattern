<?php

/**
 * @file
 * Hooks provided by the field_formatter_pattern module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define additional widget settings.
 *
 * @return array
 *   Additional widget settings.
 */
function hook_field_formatter_pattern_widget_settings() {
  return [
    'text_textarea_custom_widget' => [
      'pattern' => TRUE,
    ],
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
