<?php

namespace Drupal\field_formatter_pattern;

/**
 * The WidgetManager service.
 */
class WidgetSettings implements WidgetSettingsInterface {

  /**
   * {@inheritdoc}
   */
  public function getAllowedSettingsForAll() {
    $settings = [
      'string_textfield' => [
        'pattern' => TRUE,
        'pattern_error_message' => TRUE,
      ],
      'string_textarea' => [
        'pattern' => TRUE,
        'pattern_error_message' => TRUE,
      ],
      'text_textfield' => [
        'pattern' => TRUE,
        'pattern_error_message' => TRUE,
      ],
      'text_textarea' => [
        'pattern' => TRUE,
        'pattern_error_message' => TRUE,
      ],
      'text_textarea_with_summary' => [
        'pattern' => TRUE,
        'pattern_error_message' => TRUE,
      ],
      'key_value_textarea' => [
        'pattern' => TRUE,
        'pattern_error_message' => TRUE,
      ],
    ];
    $additional_widget_settings = \Drupal::moduleHandler()->invokeAll('field_formatter_pattern_widget_settings') ?: [];
    return $settings + $additional_widget_settings;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\field_formatter_pattern\WidgetSettingsInterface::getAllowedSettings()
   */
  public function getAllowedSettings($widget_plugin_id) {
    $all_settings = $this->getAllowedSettingsForAll();
    if (!empty($all_settings[$widget_plugin_id])) {
      return $all_settings[$widget_plugin_id];
    }
    return [];
  }

}
